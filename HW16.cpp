﻿#include <iostream>
#include <cmath>
using namespace std;


class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        cout << x << ' ' << y << ' ' << z << '\n';
    }
    void module()
    {
        cout << "module vector: " << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }
private:
    double x, y, z;
};

int main()
{
    Vector v(3, 3, 3);
    v.Show();
    v.module();
    
}


